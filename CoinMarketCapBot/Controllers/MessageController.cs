﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;
using CoinMarketCapBot.Models;
using CoinMarketCapBot.Abstractions;

namespace CoinMarketCapBot.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class MessageController : ControllerBase
    {
        private IBot _bot;
        public MessageController(IBot telegramBot)
        {
            _bot = telegramBot;
        }
        public async Task<OkResult> Update([FromBody] Update update)
        {
            // REVIEW: dont put logic in the contoller
            // just service call 
            var Comands = _bot.GetComands();
            var Message = update.Message;
            var Client = await _bot.GetClient();
            foreach (var comand in Comands)
            {
                if (comand.Contains(Message.Text))
                {
                    comand.Execute(Message, Client);
                    break;
                }
            }
            return Ok();
        }
    }
}
