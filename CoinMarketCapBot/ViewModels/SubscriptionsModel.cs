﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace CoinMarketCapBot.ViewModels
{
    public class SubscriptionsModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid CryptocurrencyId { get; set; }
        public Types? Type { get; set; }
        public TimeSpan Time { get; set; }
    }
}
