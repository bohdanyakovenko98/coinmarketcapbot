﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinMarketCapBot.ViewModels
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public long ChatId { get; set; }
    }
}
