﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinMarketCapBot.ViewModels
{
    public class CryptocurrencyModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Percent_change_24h { get; set; }
        public decimal Percent_change_7d { get; set; }
        public decimal Market_cup { get; set; }
        public decimal Volume_24h { get; set; }
        public decimal Circulating_supply { get; set; }
    }
}
