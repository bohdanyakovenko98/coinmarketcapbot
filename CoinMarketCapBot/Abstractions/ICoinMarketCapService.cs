﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoinMarketCapBot.ViewModels;

namespace CoinMarketCapBot.Abstractions
{
    // REVIEW: Should be in services
    public interface ICoinMarketCapService
    {
        public CryptocurrencyJsonModel MakeApiCall(string start, string limit, string convert);
    }
}
