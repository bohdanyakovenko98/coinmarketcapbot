﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoinMarketCapBot.ViewModels;
using Data;

namespace CoinMarketCapBot.Abstractions
{
    // REVIEW: Should be in services
    public interface ICryptocurrencyService
    {
        public Task<Cryptocurrency> GetCryptocurrencyById(Guid id);
        public Task<CryptocurrencyModel> GetCryptocurrencyByName(string name);
        public Task<CryptocurrencyModel> AddCryptocurrency(CryptocurrencyModel cryptocurrencyModel);
        public Task<CryptocurrencyModel> DeleteCryptocurrencyById(Guid id);
        public Task<CryptocurrencyModel> ChangeCryptocurrency(CryptocurrencyModel cryptocurrencyModel);
    }
}
