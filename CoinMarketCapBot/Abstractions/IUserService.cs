﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoinMarketCapBot.ViewModels;
using Data;
namespace CoinMarketCapBot.Abstractions
{
    // REVIEW: Should be in services
    public interface IUserService
    {
        public Task<User> GetUserByUserId(Guid id);
        public Task<UserModel> DeleteUserById(Guid id);
        public Task<UserModel> CreateUser(UserModel userModel);

    }
}
