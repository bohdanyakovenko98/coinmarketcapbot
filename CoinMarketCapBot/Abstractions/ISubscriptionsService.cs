﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoinMarketCapBot.ViewModels;
using Data;

namespace CoinMarketCapBot.Abstractions
{
    public interface ISubscriptionsService
    {
        public Task<Subscriptions> GetSubscriptionsById(Guid id);
        public IEnumerable<Subscriptions> GetSubscriptionsByUserId(Guid userId);
        public Task<SubscriptionsModel> CreateSubscriptions(SubscriptionsModel subscriptionsModel);
        public Task<SubscriptionsModel> DeleteSubscriptionsById(Guid id);
        public Task<SubscriptionsModel> DeleteSubscriptionsByUserId(Guid userId);
        public Task<SubscriptionsModel> DeleteSubscriptionsCryptocurrencyId(Guid cryptocurrencyId);
        public Task<SubscriptionsModel> ChangeSubscriptions(SubscriptionsModel subscriptionsModel);
    }
}
