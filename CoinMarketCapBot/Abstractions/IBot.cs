﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using CoinMarketCapBot.Models.Commands;

namespace CoinMarketCapBot.Abstractions
{
    public interface IBot
    {
        // REVIEW: dont bind abstraction to concrete realization, you need to wrap TelegramBotClient by your own bot implementation
        // use Adapter pattern
        public Task<TelegramBotClient> GetClient();
        public IReadOnlyList<Comand> GetComands();
    }
}
