﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using CoinMarketCapBot.ViewModels;

namespace CoinMarketCapBot.Abstractions
{
    public interface IParseService
    {
        // REVIEW: Why argument does not exists in this method?
        public CryptocurrencyJsonModel Parse();
    }
}
