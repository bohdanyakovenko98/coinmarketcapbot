﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using CoinMarketCapBot.Abstractions;
using CoinMarketCapBot.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace CoinMarketCapBot.Models
{
    public class CryptocurrencyService : ICryptocurrencyService
    {
        // REVIEW: Should be 'private readonly ApplicationDbContext _db;'
        private ApplicationDbContext _db { get; }
        public CryptocurrencyService(ApplicationDbContext context)
        {
            _db = context;
        }
        private CryptocurrencyModel MapCryptocurrencyModel(Cryptocurrency cryptocurrency)
        {
            return new CryptocurrencyModel
            {
                Id = cryptocurrency.Id,
                Name = cryptocurrency.Name,
                Price = cryptocurrency.Price,
                // REVIEW: Use PascalCase, not _ 
                Percent_change_24h = cryptocurrency.Percent_change_24h,
                Percent_change_7d = cryptocurrency.Percent_change_7d,
                Market_cup = cryptocurrency.Market_cup,
                Volume_24h = cryptocurrency.Volume_24h,
                Circulating_supply = cryptocurrency.Circulating_supply
            };
        }
        public async Task<Cryptocurrency> GetCryptocurrencyById(Guid id)
        {
            var cryptocurrency = await _db.Cryptocurrencys.SingleOrDefaultAsync(cryptocurrency => cryptocurrency.Id == id);
            if (cryptocurrency == null)
                // REVIEW: I think we should have out own Exceptions for multiple cases.
                throw new ArgumentException("Cryptocurrency is not found");

            return cryptocurrency;
        }
        public async Task<CryptocurrencyModel> GetCryptocurrencyByName(string name)
        {
                                                                                                  // REVIEW: cryptocurrency.Name.ToUpper() == name.ToUpper()
            var cryptocurrency = await _db.Cryptocurrencys.SingleOrDefaultAsync(cryptocurrency => cryptocurrency.Name == name);
            if (cryptocurrency == null)
                throw new ArgumentException("Cryptocurrency is not found");
            return MapCryptocurrencyModel(cryptocurrency);
        }
        public async Task<CryptocurrencyModel> AddCryptocurrency(CryptocurrencyModel cryptocurrencyModel)
        {
            var cryptocurrency = await _db.Cryptocurrencys.SingleOrDefaultAsync(cryptocurrency => cryptocurrency.Name == cryptocurrencyModel.Name);
            if (cryptocurrency != null)
                                            // REVIEW: Cryptocurrency already exists
                throw new ArgumentException("Cryptocurrency was found");

            var createdCryptocurrency = new Cryptocurrency
            {
                Id = cryptocurrencyModel.Id,
                Name = cryptocurrencyModel.Name,
                Price = cryptocurrencyModel.Price,
                Percent_change_24h = cryptocurrencyModel.Percent_change_24h,
                Percent_change_7d = cryptocurrencyModel.Percent_change_7d,
                Market_cup = cryptocurrencyModel.Market_cup,
                Volume_24h = cryptocurrencyModel.Volume_24h,
                Circulating_supply = cryptocurrencyModel.Circulating_supply
            };
            await _db.Cryptocurrencys.AddAsync(createdCryptocurrency);
            await _db.SaveChangesAsync();
            return MapCryptocurrencyModel(createdCryptocurrency);
        }

        public async Task<CryptocurrencyModel> DeleteCryptocurrencyById(Guid id)
        {
            var cryptocurrency = await GetCryptocurrencyById(id);
            _db.Cryptocurrencys.Remove(cryptocurrency);
            await _db.SaveChangesAsync();
            return MapCryptocurrencyModel(cryptocurrency);
        }
        // REVIEW: You need separate model UpdateCryptuCurrencyModel for updating
        public async Task<CryptocurrencyModel> ChangeCryptocurrency(CryptocurrencyModel cryptocurrencyModel)
        {
            var foundCryptocurrency = await GetCryptocurrencyById(cryptocurrencyModel.Id);
            foundCryptocurrency.Name = cryptocurrencyModel.Name;
            foundCryptocurrency.Price = cryptocurrencyModel.Price;
            foundCryptocurrency.Percent_change_24h = cryptocurrencyModel.Percent_change_24h;
            foundCryptocurrency.Percent_change_7d = cryptocurrencyModel.Percent_change_7d;
            foundCryptocurrency.Market_cup = cryptocurrencyModel.Market_cup;
            foundCryptocurrency.Volume_24h = cryptocurrencyModel.Volume_24h;
            foundCryptocurrency.Circulating_supply = cryptocurrencyModel.Circulating_supply;
            await _db.SaveChangesAsync();
            return MapCryptocurrencyModel(foundCryptocurrency);
        }
    }
}
