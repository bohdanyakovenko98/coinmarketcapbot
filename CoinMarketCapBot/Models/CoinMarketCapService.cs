﻿using System;
using System.Net;
using System.Web;
using CoinMarketCapBot.Abstractions;
using CoinMarketCapBot.ViewModels;
using Newtonsoft.Json;

namespace CoinMarketCapBot.Models
{
    public class CoinMarketCapService : ICoinMarketCapService
    {
        private string Api { get; }
        private string Url { get; }
        CoinMarketCapService(string apiKey, string URL)
        {
            Api = apiKey;
            Url = URL;
        }
        public CryptocurrencyJsonModel MakeApiCall(string start, string limit, string convert)
        {
            var URL = new UriBuilder(Url);

            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString["start"] = start;
            queryString["limit"] = limit;
            queryString["convert"] = convert;

            URL.Query = queryString.ToString();

            // REVIEW: use HttpClient instead of WebClient
            var client = new WebClient();
            client.Headers.Add("X-CMC_PRO_API_KEY", Api);
            client.Headers.Add("Accepts", "application/json");
            CryptocurrencyJsonModel model = JsonConvert.DeserializeObject<CryptocurrencyJsonModel>(client.DownloadString(URL.ToString()));
            return model;
        }
    }
}
