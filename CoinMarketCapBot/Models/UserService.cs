﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoinMarketCapBot.Abstractions;
using CoinMarketCapBot.ViewModels;
using Data;
using Microsoft.EntityFrameworkCore;

namespace CoinMarketCapBot.Models
{
    public class UserService : IUserService
    {
        private ApplicationDbContext _db { get; }
     
        public UserService(ApplicationDbContext context)
        {
            _db = context;
        }
        private UserModel MapUserModel(User user)
        {
            return new UserModel
            {
                Id = user.Id,
                ChatId = user.ChatId
            };
        }
        public async Task<User> GetUserByUserId(Guid id)
        {
            var user = await _db.User.SingleOrDefaultAsync(user => user.Id == id);
            if (user == null)
                throw new ArgumentException("User is not found. Check input");

            return user;
        }
        public async Task<UserModel> DeleteUserById(Guid id)
        {
            var user = await GetUserByUserId(id);
            _db.User.Remove(user);
            await _db.SaveChangesAsync();
            return  MapUserModel(user);
        }
        public async Task<UserModel> CreateUser(UserModel userModel)
        {
            var user = await _db.User.SingleOrDefaultAsync(user => user.ChatId == userModel.ChatId);
            if (user != null)
                throw new ArgumentException("User was found");
           
            var createdUser = new User
            {
                Id = userModel.Id,
                ChatId = userModel.ChatId
            };
            await _db.User.AddAsync(createdUser);
            await _db.SaveChangesAsync();
            return MapUserModel(createdUser);
        }
    }
}
