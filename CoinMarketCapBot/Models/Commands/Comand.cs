﻿using Telegram.Bot.Types;
using Telegram.Bot;

namespace CoinMarketCapBot.Models.Commands
{
    // REVIEW: Should be in models
    public abstract class Comand
    {
        public abstract string BotName { get; set; }
        Comand(string bot)
        {
            BotName = bot;
        }
        public abstract string Name { get; }
        public abstract void Execute(Message message, TelegramBotClient client);
        public bool Contains(string comand)
        {
            return comand.Contains(this.Name) && comand.Contains(BotName);
        }
    }
}
