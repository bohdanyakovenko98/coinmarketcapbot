﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using CoinMarketCapBot.Models.Commands;
namespace CoinMarketCapBot.Models
{
    public class Bot
    {
        private static TelegramBotClient Client;
        private string ApiKey;
        private List<Comand> commandsList;

        public  IReadOnlyList<Comand> Comands { get => commandsList.AsReadOnly(); }

        Bot(string api)
        {
            ApiKey = api;
        }

        public async Task<TelegramBotClient> GetClient()
        {
            if (Client!=null)
            {
                return Client;
            }
            commandsList = new List<Comand>();
            //add comand
            Client = new TelegramBotClient(ApiKey);
            Client.SetWebhookAsync("");

            return Client;
        }
        // REVIEW: ComMands
        public IReadOnlyList<Comand> GetComands()
        {
            return Comands;
        }
    }
}
