﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using CoinMarketCapBot.Abstractions;
using CoinMarketCapBot.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace CoinMarketCapBot.Models
{
    public class SubscriptionsService : ISubscriptionsService
    {
        private ApplicationDbContext _db { get; }
        public SubscriptionsService(ApplicationDbContext context)
        {
            _db = context;
        }
        private SubscriptionsModel MapSubscriptionsModel(Subscriptions subscriptions)
        {
            // REVIEW: Should be in SubsriptionModel
            return new SubscriptionsModel
            {
                Id = subscriptions.Id,
                UserId = subscriptions.UserId,
                CryptocurrencyId = subscriptions.CryptocurrencyId,
                Type = subscriptions.Type,
                Time = subscriptions.Time
            };
        }
        public async Task<Subscriptions> GetSubscriptionsById(Guid id)
        {
            var subscriptions = await _db.Subscriptions.SingleOrDefaultAsync(subscriptions => subscriptions.Id == id);
            if (subscriptions == null)
                throw new ArgumentException("User is not found. Check input");

            return subscriptions;
        }

        public IEnumerable<Subscriptions> GetSubscriptionsByUserId(Guid userId)
        {
            var subscriptions = from s in _db.Subscriptions
                             join u in _db.User on s.UserId equals u.Id
                             join c in _db.Cryptocurrencys on s.CryptocurrencyId equals c.Id
                             where userId == u.Id
                             select s;

            return subscriptions;
        }
        public async Task<SubscriptionsModel> CreateSubscriptions(SubscriptionsModel subscriptionsModel)
        {
            var subscriptions = await _db.Subscriptions.SingleOrDefaultAsync(subscriptions => subscriptions.Id == subscriptionsModel.Id);
            if (subscriptions != null)
                throw new ArgumentException("Subscriptions was found");

            var createdSubscriptions = new Subscriptions
            {
                Id = subscriptionsModel.Id,
                UserId = subscriptionsModel.UserId,
                CryptocurrencyId = subscriptionsModel.CryptocurrencyId,
                Type = subscriptionsModel.Type,
                Time = subscriptionsModel.Time
            };
            await _db.Subscriptions.AddAsync(createdSubscriptions);
            await _db.SaveChangesAsync();
            return MapSubscriptionsModel(createdSubscriptions);
        }
        public async Task<SubscriptionsModel> DeleteSubscriptionsById(Guid id)
        {
            var subscriptions = await GetSubscriptionsById(id);
            _db.Subscriptions.Remove(subscriptions);
            await _db.SaveChangesAsync();
            return MapSubscriptionsModel(subscriptions);
        }
        public async Task<SubscriptionsModel> DeleteSubscriptionsByUserId(Guid userId)
        {
            var subscriptions = await _db.Subscriptions.SingleOrDefaultAsync(subscriptions => subscriptions.UserId == userId);
            if (subscriptions == null)
                throw new ArgumentException("Subscriptions was not found");
            _db.Subscriptions.Remove(subscriptions);
            await _db.SaveChangesAsync();
            return MapSubscriptionsModel(subscriptions);

        }
        public async Task<SubscriptionsModel> DeleteSubscriptionsCryptocurrencyId(Guid cryptocurrencyId)
        {
            // REVIEW: Always add empty line between logic blocks, it make code more readable
            var subscriptions = await _db.Subscriptions.SingleOrDefaultAsync(subscriptions => subscriptions.CryptocurrencyId == cryptocurrencyId);
            if (subscriptions == null)
                throw new ArgumentException("Subscriptions was not found");
            _db.Subscriptions.Remove(subscriptions);
            await _db.SaveChangesAsync();
            return MapSubscriptionsModel(subscriptions);
        }
        public async Task<SubscriptionsModel> ChangeSubscriptions(SubscriptionsModel subscriptionsModel)
        {
            var foundSubscriptions = await GetSubscriptionsById(subscriptionsModel.Id);
            foundSubscriptions.UserId = subscriptionsModel.UserId;
            foundSubscriptions.CryptocurrencyId = subscriptionsModel.CryptocurrencyId;
            foundSubscriptions.Type = subscriptionsModel.Type;
            foundSubscriptions.Time = subscriptionsModel.Time;
            await _db.SaveChangesAsync();
            return MapSubscriptionsModel(foundSubscriptions);
        }
    }
}
