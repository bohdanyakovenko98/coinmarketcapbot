﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinMarketCapBot.Model
{
    // REVIEW: I was already saw it
    // dont duplicate code
    [JsonObject]
    public class CryptocurrencyModel
    {
        [JsonProperty("data")]
        public IEnumerable<Data> Cryptocurrency { get; set; }

    }

    [JsonObject]
    public class Data
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("quote")]
        public Quote quote { get; set; }

        [JsonProperty("circulating_supply")]
        public decimal circulating_supply { get; set; }
    }

    [JsonObject]
    public class Quote
    {
        [JsonProperty("USD")]
        public Convert usd { get; set; }

    }

    [JsonObject]
    public class Convert
    {
        [JsonProperty("price")]
        public decimal price { get; set; }

        [JsonProperty("percent_change_24h")]
        public decimal percent_change_24h { get; set; }

        [JsonProperty("percent_change_7d")]
        public decimal percent_change_7d { get; set; }

        [JsonProperty("market_cap")]
        public decimal market_cup { get; set; }

        [JsonProperty("volume_24h")]
        public decimal volume_24h { get; set; }
    }
}
