﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    public class User
    {
        public Guid Id { get; set; }
        public long ChatId { get; set; }
        // REVIEW: Seems subscription must contain userId, instead of user list of subscriptions, what do you think?
        public List<Subscriptions> Subscriptions { get; set; }
    }
}
