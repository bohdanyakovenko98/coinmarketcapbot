﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    public class Subscriptions
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public Guid CryptocurrencyId { get; set; }
        public Cryptocurrency Cryptocurrency { get; set; }
        public Types? Type { get; set; }
        public TimeSpan Time { get; set; }

    }
    // REVIEW: use better naming, i dont understand about what 'Types' are we talking about
    // If other developer meets Type Property with type Types somewhere, i think he wont understand what is the property
    public enum Types
    {
        // REVIEW: Use PascalCase for enums
        everyday = 1,
        everytime = 2,
    }
}
