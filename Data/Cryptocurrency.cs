﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    // REVIEW: Maybe just Currency?
    public class Cryptocurrency
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        // REVIEW: Fix naming to pascal case
        public decimal Percent_change_24h { get; set; }
        public decimal Percent_change_7d { get; set; }
        public decimal Market_cup { get; set; }
        public decimal Volume_24h { get; set; }
        public decimal Circulating_supply { get; set; }
        public List<Subscriptions> Subscriptions { get; set; }
    }
}
